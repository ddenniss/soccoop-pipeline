(ns soccoop.view-parsed-products
  (:require
   [selmer.parser :as selmer]
   [soccoop.config :refer [config]]))

(defn make-html-page [html data]
  (let [template-html-file (str html ".template")
        template-html (slurp template-html-file)]
    (spit html (selmer/render template-html data))))
