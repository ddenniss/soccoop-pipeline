(ns soccoop.parser
  (:require
   [clojure.set :as set]
   [etaoin.api :as e]
   [soccoop.config :refer [config]]
   [soccoop.retailers :as retailers]))

(defn- get-html [driver url]
  (e/go driver url)
  (e/get-source driver))

(def ^:private browser-cfg (-> config :webdriver :opts))

(defn parse-pages [urls]
  (e/with-firefox
    browser-cfg
    driver
    (reduce
      (fn [products url]
        (conj
          products
          (retailers/parse-page
            {:url url :html (get-html driver url)})))
      []
      urls)))

(defn parse-pages-w-cache [urls]
  (let [cached-product-info-file (-> config :data-out :product-info :edn)
        cached-product-info (try
                              (-> cached-product-info-file
                                  slurp
                                  read-string)
                              (catch java.io.IOException _ nil))
        processed-urls (map :url cached-product-info)
        unprocessed-urls (vec
                           (set/difference
                             (set urls)
                             (set processed-urls)))]
    (e/with-firefox
      browser-cfg
      driver
      (reduce
        (fn [products url]
          (do
            (spit cached-product-info-file products)
            (conj
              products
              (retailers/parse-page
                {:url url :html (get-html driver url)}))))
        cached-product-info
        unprocessed-urls))))
