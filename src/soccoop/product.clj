(ns soccoop.product
  (:require
    [clojure.spec.alpha :as s]
    [spec-dict :refer :all]))

(defn- url? [s]
  (re-matches #"http(s)://(.+)\.(.+){2,}" s))

(defn- today []
  (.format (java.text.SimpleDateFormat. "yyyy/MM/dd") (java.util.Date.)))

(s/def ::nestring (s/and string? not-empty))

(s/def ::product
  {:url (s/nilable url?)
   :hash int?
   :title ::nestring
   :price (s/nilable string?)
   :weight (s/nilable string?)})

(defn make-product [url title price weight]
  {
    :url url
    :hash (hash (str title price weight))
    :date (today)
    :title title
    :price price
    :weight weight})
