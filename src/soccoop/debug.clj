(ns soccoop.debug
  (:require
   [clojure.pprint :as p]
   [clojure.set :as set]
   [etaoin.api :as e]
   [odysseus.csv :as csv]
   [soccoop.parser :as parser]
   [soccoop.config :refer [config]])
  (:gen-class))

(defn get-urls [filepath]
  (as->
    filepath
    $
    (csv/read-csv $ {:separator \tab :header true})
    (map #(% "Ссылка на продукт") $)
    (remove nil? $)))

(defn -main [& _]
  (let [urls (->> config :data-in :product-table get-urls)
        product-info (parser/parse-pages (take 3 urls))]
    (p/pprint product-info)
    true))
