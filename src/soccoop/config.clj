(ns soccoop.config)

(def config {
               :webdriver
               {
                  :opts
                  {
                     :headless true
                     :args ["--disable-blink-features" "AutomationControlled"
                            "--disable-extensions"
                            "--disable-application-cache"]
                     ;; OPTIMIZE Геморр: Когда на новой системе запускаете браузер, необходимо выбрать путь для профиля firefox и прописать его сюда в :profile. Затем вручную открыть браузер под этим профилем (можно через about:profiles) по адресу e.g. https://www.okeydostavka.ru/msk/bakaleia-i-konservy/krupy-i-bobovye/gorokh-tchn-okei-daily-shlifovannyi-800g. После чего в профиль записывается правильное состояние браузера и сайт начинает пускать его без 403 в авто-режиме. Нужно понять, какие именно параметры в профиле (какие файлы) обеспечивают такое поведение и закоммитить их в репу robocoop. Задиффить директорию до и директорию после?
                     ;; Detected: Cookies! А именно файл cookies.sqlite. Сессия хранится ограниченное время, после чего сайт снова показывает фак браузеру. Найти соответствующий кукиз и поменять ему время жизни на побольше? (куки qrator_jsid?)
                     :profile "/home/denis/settings/robocoop/firefox/profile2_before"}}

               :data-in
               {
                  :product-table "data/data_in/soccoop.prices.okey.tsv"
                  :product-page-samples-dir "var/okeydostavka_pages"
                  :gs-table-url "https://docs.google.com/spreadsheets/d/1aPZnwe-aBJRXvc10HtjHvxjAn9TwKkCchBvGqCZNJko/edit#gid=377092909"}

               :data-out
               {
                  :product-info
                  {
                    :edn "data/data_out/product_info.edn"
                    :html "resources/html/products_okeydostavka.html"}}

               :cache
               {
                  :product-card-pages "var/product_card_pages.edn"}})
